SUBJECT: Review for COMLAN Special Issue for Trends in Functional Programming 2013/14

Dear colleague,

Jurriaan Hage and I are guest editing a special issue of COMLAN that
focuses on a selection of revised best papers from Trends in
Functional Programming 2013 and 2014. Part of this process is a peer
review of the revised publications. We think that you would be a great
person to participate in this process. The review will take place
between May 23rd, 2016 and November 28th, 2016---the first date being
the submission deadline for the authors and the last date being their
notification date. We hope that you will review one paper out of a
batch of six.

At your earliest convenience, please let us know if you are able to
help us in this way.

Thank you so much,

Jur & Jay
