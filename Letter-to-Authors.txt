SUBJECT: COMLAN Special Issue for Trends in Functional Programming 2013/14

Dear author,

We've got permission from COMLAN to have a Special Issue for Trends in
Functional Programming 2013/14! At this point, you should prepare the
revised versions of your paper. You will have to submit by May 23rd,
2016. We will do the review by November 28th, 2016. We expect that the
publication will be on February 27th, 2017.

To submit your revision, go to

XXX

and submit with "SI:TFP 2013/14" as the article type.

We're excited to work with you on this!

Regards,

Jurr & Jay
